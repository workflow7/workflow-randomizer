# Workflow

- Parent project: git@gitlab.com:cintegrate/workflow.git

## Goal

Showcase working with pieces of code in separate repositories.

## Randomizer

Basic example piece of code that returns a randomized string with variable length.

## Usage
- Clone this project
  - Create git post-commit hook
    - Point to
- Create a branch
- Set up the git@gitlab.com:cintegrate/workflow.git project
  - Link the composer.json in workflow.git project to point to workflow-randomizer.git branch
    - "cintegrate/workflow.git" : "dev-[name-of-created-branch]"
- Change character list in src/Randomizer.php:12
  - for example: $characterList = '12345678';
- 