<?php
declare(strict_types=1);

namespace Workflow\Randomizer;

class Randomize
{
    public static function length(int $length): string
    {
        return str_repeat('a', $length);
    }
}