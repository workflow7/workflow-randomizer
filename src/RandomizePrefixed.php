<?php
declare(strict_types=1);

namespace Workflow\Front;

use Workflow\Randomizer\Randomize;

class RandomizePrefixed
{
    public static function length(int $length, string $prefix): string
    {
        return $prefix . Randomize::length($length);
    }
}