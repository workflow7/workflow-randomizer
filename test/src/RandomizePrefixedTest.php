<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Workflow\Front\RandomizePrefixed;

final class RandomizePrefixedTest extends TestCase
{
    public function testLength(): void
    {
        $this->assertSame(8, strlen(RandomizePrefixed::length(5, 'aaa')));
    }
}
