<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Workflow\Randomizer\Randomize;

final class RandomizeTest extends TestCase
{
    public function testLength(): void
    {
        $this->assertSame(5, strlen(Randomize::length(5)));
    }
}
